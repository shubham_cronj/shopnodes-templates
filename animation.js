var headerBackground = document.getElementById('header');
// headerBackground = "url('images/background1.jpg')";
var isHeaderBlack = false;
var scrollEvent = function(){
	// console.log(headerBackground);

	if(document.body.scrollTop > 100 && !isHeaderBlack){
		headerBackground.style.background = "rgba(73,21,89,0.95)";
		isHeaderBlack = true;
		headerBackground.style.padding = "0px";
		headerBackground.style.boxShadow = "0px 1px 2px #491559";
		// headerBackground.style.marginBottom = "25px";
	}
	if( document.body.scrollTop < 100 && isHeaderBlack){
		headerBackground.style.background = "none";
		isHeaderBlack = false;
		headerBackground.style.padding = "25px 0px";
		headerBackground.style.boxShadow = "none";
		// headerBackground.style.marginBottom = "0";
	}
}