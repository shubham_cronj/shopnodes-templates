(function(){
	window.onload = function(){
		initialize();
	};
	function initialize(){
		var contactUsSidebar = document.getElementsByClassName('vm-contactus-sidebar')[0];
		var contactUsContent = document.getElementsByClassName('modal-content')[0];
		contactUsSidebar.addEventListener('click',function() {
			toggleContactUs(contactUsSidebar, contactUsContent);
		})
	}
	function toggleContactUs(contactUsSidebar, contactUsContent){
		if(isContactUsOpen(contactUsSidebar, contactUsContent)) {
			closeContactUs(contactUsSidebar, contactUsContent);
		}
		else{
			openContactUs(contactUsSidebar, contactUsContent);
		}
	}
	function openContactUs(contactUsSidebar, contactUsContent) {
		removeClass(contactUsSidebar, 'vm-contactus-sidebar-close');
		removeClass(contactUsContent, 'modal-content-close');
		addClass(contactUsSidebar, 'vm-contactus-sidebar-open');
		addClass(contactUsContent, 'modal-content-open');
	}
	function closeContactUs(contactUsSidebar, contactUsContent) {
		removeClass(contactUsSidebar, 'vm-contactus-sidebar-open');
		removeClass(contactUsContent, 'modal-content-open');
		addClass(contactUsSidebar, 'vm-contactus-sidebar-close');
		addClass(contactUsContent, 'modal-content-close');
	}
	function isContactUsOpen(contactUsSidebar, contactUsContent){
		if(contactUsContent.classList.contains('modal-content-close') || contactUsSidebar.classList.contains('vm-contactus-sidebar-close')) {
			return false;
		}
		return true;
	}

	function addClass(el, className) {
	  el.classList.add(className)
	}

	function removeClass(el, className) {
	  el.classList.remove(className)
	 }
})()
